const io = require('../io');
const requestJson = require('request-json');
const jwt = require('../JWT');
const baseMLabURL = "https://api.mlab.com/api/1/databases/proyect_techu/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
var fich_ibanes = require('../Listado_Ibanes.json');


//*************************ALTA DE CUENTA***************************************
//*                                                                            *
//******************************************************************************
function createAccounts(req, res) {

  var email = req.body.email;
  var timestamp = new Date();
  var timestamp_json = JSON.stringify(timestamp);
  var timestamp_object = JSON.parse(timestamp_json);
  var queryEmail = 'q={"email": "' + email + '"}';
  const iban = fich_ibanes.find(function(iban,index){
                                  return iban.Estado === undefined
                                })
  var index = fich_ibanes.indexOf(iban)
  var newAccount = {
    "email": req.body.email,
    "IBAN": iban.IBAN,
    "Balance": 0.00,
    "Badge": req.body.Badge,
    "Timestamp": timestamp_object
  }
  var newAccount_json = JSON.stringify(newAccount)
  var httpClient = requestJson.createClient(baseMLabURL);
  var token = req.headers['token']
  var verifyToken = jwt.validarToken(token);

  fich_ibanes[index].Estado = "No disponible"

  if  (!req.body.email) {
    var response = {"msg" : "Existen campos de entrada mal informados o no informados"}

    res.status(400);
    res.send(response);

    return;
  }

  if (verifyToken.status != 200) {
    res.status(verifyToken.status)
    res.send({"msg" : verifyToken.msj})

    return

  } else {
      if (email != verifyToken.email) {
        res.status(401)
        res.send({"msg" : "Error en integridad del mensaje, sesión no corresponde al usuario conectado"})

        return
      }
    }

  httpClient.get("Users?" + queryEmail + "&" + mLabAPIKey,
        function (errGet, resMLabGet, bodyGet) {

            if (errGet || resMLabGet.statusCode != 200){
              var response = {"msg" : "Error validación si usuario ya dado de alta"}

              res.status(500);
              res.send(response)

            } else{
                  if (bodyGet.length > 0) {
                      httpClient.post("Accounts?" + mLabAPIKey,newAccount,
                          function (errPost, resMLabPost, bodyPost) {

                              if (errPost || resMLabPost.statusCode != 200){
                                  res.status(500);
                                  res.send({"msg" : "Error insertando cuenta"})

                              } else {
                                  io.writeUserDatatoFile(fich_ibanes)

                                  res.status(200)

                                  var response = {"msg" : "Cuenta creada", "IBAN" : iban.IBAN }

                                  res.send(response)
                                }
                          }
                      )

                  } else {
                    res.status(404);

                    var response = {" msg" : "Usuario no encontrado"};

                    res.send(response);
                    }
              }
        }
  )
}

function getCuentasByMail(req, res){
  var email = req.params.email;
  var query = 'q={"email":"'+ email + '"}';
  var httpClient = requestJson.createClient(baseMLabURL);
  var token = req.headers['token']
  var verifyToken = jwt.validarToken(token);

  if  (!email) {
    var response = {"msg" : "Existen campos de entrada mal informados o no informados"}

    res.status(400);
    res.send(response);

    return;
  }

  if (verifyToken.status != 200) {
    res.status(verifyToken.status)
    res.send({"msg" : verifyToken.msj})

    return

  } else {
      if (email != verifyToken.email) {
        res.status(401)
        res.send({"msg" : "Error en integridad del mensaje, sesión no corresponde al usuario conectado"})

        return
      }
    }

  httpClient.get("Accounts?" + query + "&" + mLabAPIKey,
        function (err, resMLab, body) {
          if (err){
              var response = {
                  "msg" : "Error obteniendo las cuentas del usuario"
              }

            res.status(500);
            res.send(response);

          } else {
              if (body.length > 0){
                var response = body;

                res.status(200);
                res.send(response);

              } else {
                    var response = {
                      "msg" : "Usuario sin cuentas"
                    }
                    res.status(404);
                    res.send(response);
                }
            }
        }
  )
}

module.exports.getCuentasByMail = getCuentasByMail;
module.exports.createAccounts = createAccounts;
