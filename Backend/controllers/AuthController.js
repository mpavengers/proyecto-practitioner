const requestJson = require('request-json');
const crypt = require('../crypt');
const jwt = require('../JWT');
const baseMLabURL = "https://api.mlab.com/api/1/databases/proyect_techu/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

//*******************************LOGIN DE USUARIO*******************************
//*                                                                            *
//******************************************************************************
function postLogin(req, res){

  var fecha_entrada = new Date();
  var fecha_entrada_milisegundos = fecha_entrada.getTime()
  var email = req.body.email;
  var passplain = req.body.Password;
  var query = 'q={"email": "' + email + '"}';
  var httpClient = requestJson.createClient(baseMLabURL);
  var payload = {mail:email}
  var token = jwt.createToken(payload);

  if  (!email || !passplain) {
   var response = {"msg" : "Existen campos de entrada mal informados o no informados"}

   res.status(400);
   res.send(response);

   return;
  }

  httpClient.get("Users?" + query + "&" + mLabAPIKey,
          function (err, resMLab, bodyGet) {
            if (err || resMLab.statusCode != 200){
              var response = {"msg" : "Error obteniendo usuario"}

              res.status(500);
              res.send(response)

          } else {
            if (bodyGet.length > 0) {
              var datusuario = bodyGet[0];

              result = crypt.checkPassword(passplain, bodyGet[0].Password);

              if (result){
                var timestamp = new Date();
                var timestamp_json = JSON.stringify(timestamp)
                var timestamp_object = JSON.parse(timestamp_json)
                var fecha = new Date (timestamp_object)
                var fecha_update_milisegundos = fecha.getTime()
                var diferencia = fecha_entrada_milisegundos - fecha_update_milisegundos

                var putBody = '{"$set":{"logged":true,"Last_Login":' + timestamp_json + '}}';

                httpClient.put("Users?" + query + "&" + mLabAPIKey,JSON.parse(putBody),
                      function (errPUT, resMLabPUT, bodyPUT) {
                          if (errPUT || resMLabPUT.statusCode != 200){

                              res.status(500);
                              res.send({"msg" : "error bbdd actualizando usuario", "emailUsuario" : datusuario.email})

                          } else {
                              res.status(200);
                              res.setHeader('token', token);
                              res.send({
                                "loged": true,
                                "mensaje" : "Login correcto",
                                "nombre": datusuario.first_name,
                                "apellidos": datusuario.last_name,
                                "emailUsuario" : datusuario.email});
                            }
                      }
                )

              } else{
                  res.status(401);
                  res.send({"msg":"Password incorrecta"});
                }

            } else {
                res.status(404);

                var response = {"msg" : "Usuario no encontrado"};

                res.send(response);
              }
            }
        }
   )
  }


  function postLogout(req, res) {
    var email=req.body.email;
    var query = 'q={"email": "' + email + '"}'
    var httpClient = requestJson.createClient(baseMLabURL);
    var token = req.headers['token']
    var verifyToken = jwt.validarToken(token);

    if  (!email) {
      var response = {"msg" : "Existen campos de entrada mal informados o no informados"}

      res.status(400);
      res.send(response);

      return;
    }

    if (verifyToken.status != 200) {
      res.status(verifyToken.status)
      res.send({"msg" : verifyToken.msj})

      return

    } else {
        if (email != verifyToken.email) {
          res.status(401)
          res.send({"msg" : "Error en integridad del mensaje, sesión no corresponde al usuario conectado"})

          return
        }
      }

    httpClient.get("Users?" + query + "&" + mLabAPIKey,
          function (errGet, resMLabGet, bodyGet) {
            if (errGet || resMLabGet.statusCode != 200){
                res.status(500);
                res.send({"msg" : "Error logout al usuario"})

            } else {

                if (bodyGet.length > 0 && bodyGet[0].logged) {
                  var datusuario = bodyGet[0];
                  var timestamp = new Date();
                  var timestamp_json = JSON.stringify(timestamp)
                  var putBody = '{"$unset":{"logged":""},"$set":{"Last_Conecction":' + timestamp_json + '}}'
                  var putBody2 = '{"$set":{"Last_Conecction":' + timestamp_json + '}}'

                  httpClient.put("Users?" + query + "&" + mLabAPIKey,JSON.parse(putBody),
                        function (errPut, resMLabPut, bodyput) {
                            if (errPut || resMLabPut.statusCode != 200){
                                res.status(500);
                                res.send({"msg" : "Error actualizando logout al usuario"})

                            } else {
                                res.status(200);

                                res.send({"msg" : "Logout correcto","emailUsuario" : datusuario.email});
                              }
                        }
                  )

                } else {
                    if (bodyGet.length > 0 && bodyGet.logged != true) {
                      res.status(401);

                      var response = {" msg" : "Usuario no logado"};

                      res.send(response);

                    } else{
                        res.status(404);

                        var response = {" msg" : "Usuario no pertenece a la entidad"};

                        res.send(response);
                    }
                  }
                }
          }
      )
  };

module.exports.postLogin= postLogin;
module.exports.postLogout = postLogout;
