const requestJson = require('request-json');
const crypt = require('../crypt');
const jwt = require('../JWT');
const baseMLabURL = "https://api.mlab.com/api/1/databases/proyect_techu/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

//*******************************ALTA DE USUARIO********************************
//*                                                                            *
//******************************************************************************
function createUsers(req, res) {
  var email = req.body.email;
  var payload = {mail:email}
  var token = jwt.createToken(payload);
  var queryID = 'f={"id": 1,_id:0}&s={"id": -1}&l=1';
  var queryEmail = 'q={"email": "' + email + '"}';
  var httpClient = requestJson.createClient(baseMLabURL);

  if (!req.body.email || !req.body.Password || !req.body.Tittle || !req.body.first_name || !req.body.last_name || !req.body.DNI_Passport ||
      !req.body.Birth_date || !req.body.Phone || !req.body.gender || !req.body.Street_Name || !req.body.Street_Number ||
      !req.body.City || !req.body.Country) {

        var response = {"msg" : "Existen campos de entrada mal informados o no informados"}

        res.status(400);
        res.send(response);

        return;
      }

  httpClient.get("Users?" + queryEmail + "&" + mLabAPIKey,
        function (errGet, resMLabGet, bodyGet) {
            if (errGet || resMLabGet.statusCode != 200){
              var response = {"msg" : "Error validadon si usuario ya dado de alta"}

              res.status(500);
              res.send(response)

            } else{
                  if (bodyGet.length > 0) {
                      res.status(409);

                      var response = {"msg" : "Usuario ya esta dado de alta en la entidad"};

                      res.send(response);
                  } else {
                  httpClient.get("Users?" + queryID + "&" + mLabAPIKey,
                        function (errId, resMLabId, bodyId) {

                          if (errId || resMLabId.statusCode != 200){
                            var response = {"msg" : "Error obteniendo Id último"}

                            res.status(500);
                            res.send(response)

                          } else {
                              if (bodyId[0].id == undefined) {
                                newId = 1
                              } else {
                                newId = bodyId[0].id + 1
                              }

                              var newUser = {
                                  "id": newId,
                                  "email": req.body.email,
                                  "Password": crypt.hash(req.body.Password),
                                  "Tittle": req.body.Tittle,
                                  "first_name": req.body.first_name,
                                  "last_name": req.body.last_name,
                                  "DNI_Passport": req.body.DNI_Passport,
                                  "Birth_date": req.body.Birth_date,
                                  "Phone": req.body.Phone,
                                  "gender": req.body.gender,
                                  "Street_Name": req.body.Street_Name,
                                  "Street_Number": req.body.Street_Number,
                                  "City": req.body.City,
                                  "Country": req.body.Country
                              }

                              httpClient.post("Users?" + mLabAPIKey, newUser,
                                    function (errNew, resMLabNew, bodyNew) {
                                      if (errNew || resMLabNew.statusCode != 200){
                                        var response = {"msg" : "Error en alta usuario"}

                                        res.status(500);
                                        res.send(response)

                                      } else {
                                          res.setHeader('token', token);
                                          res.status(201).send({"msg":"Usuario creado"});
                                        }
                                    }
                              )
                            }
                        }
                    )
                    }
              }
        }
    )
}

function getUserByMail(req, res) {
  var email = req.params.email;
  var query = 'q={"email":"'+ email + '"}';
  var token = req.headers['token']
  var httpClient = requestJson.createClient(baseMLabURL);
  var verifyToken = jwt.validarToken(token);

  if  (!email) {
    var response = {"msg" : "Existen campos de entrada mal informados o no informados"}

    res.status(400);
    res.send(response);

    return;
  }

  if (verifyToken.status != 200) {
    res.status(verifyToken.status)
    res.send({"msg" : verifyToken.msj})

    return

  } else {
      if (email != verifyToken.email) {
        res.status(401)
        res.send({"msg" : "Error en integridad del mensaje, sesión no corresponde al usuario conectado"})

        return
      }
    }

  httpClient.get("Users?" + query + "&" + mLabAPIKey,
        function (err, resMLab, body) {

          if (err){
              var response = {
                  "msg" : "Error obteniendo datos del usuario"
              }

            res.status(500);

          } else {
              if (body.length > 0){
                var response =  {
                                  "email": body[0].email,
                                  "Tittle": body[0].Tittle,
                                  "first_name": body[0].first_name,
                                  "last_name": body[0].last_name,
                                  "DNI_Passport": body[0].DNI_Passport,
                                  "Birth_date": body[0].Birth_date,
                                  "Phone": body[0].Phone,
                                  "gender": body[0].gender,
                                  "Street_Name": body[0].Street_Name,
                                  "Street_Number": body[0].Street_Number,
                                  "City": body[0].City,
                                  "Country": body[0].Country
                                }

                res.status(200);

              } else {
                    var response = {
                      "msg" : "Usuario no dado de alta en entidad"
                    }
                    res.status(404);
                }
            }
          res.send(response);
        }
  )
}

function postUpdateUsers(req, res){

  var fecha_entrada = new Date();
  var fecha_entrada_milisegundos = fecha_entrada.getTime()
  var email_params = req.params.email;
  var email = req.body.email;
  var password = req.body.Password;
  var tittle = req.body.Tittle;
  var first_name =  req.body.first_name;
  var last_name = req.body.last_name;
  var DNI_Passport = req.body.DNI_Passport;
  var Birth_date = req.body.Birth_date;
  var Phone = req.body.Phone;
  var gender = req.body.gender;
  var Street_Name = req.body.Street_Name;
  var Street_Number = req.body.Street_Number;
  var City = req.body.City;
  var Country = req.body.Country;
  var query = 'q={"email": "' + email_params + '"}';
  var httpClient = requestJson.createClient(baseMLabURL);
  var token = req.headers['token']
  var verifyToken = jwt.validarToken(token);

  if  (!email && !password && !tittle && !first_name && !last_name && !DNI_Passport && !Birth_date && !Phone && !gender && !Street_Name &&!Street_Number && !City && !Country) {
   var response = {"msg" : "No se ha modificado ningun dato del usuario, no hay nada que actualizar."}

   res.status(400);
   res.send(response);

   return;
  }

  if (verifyToken.status != 200) {
    res.status(verifyToken.status)
    res.send({"msg" : verifyToken.msj})

    return

  } else {
        if (email_params != verifyToken.email) {
        res.status(401)
        res.send({"msg" : "Error en integridad del mensaje, sesión no corresponde al usuario conectado"})

        return
      }
    }

  httpClient.get("Users?" + query + "&" + mLabAPIKey,
          function (errGet, resMLabGet, bodyGet) {
          if (errGet || resMLabGet.statusCode != 200){
            var response = {"msg" : "Error obteniendo usuario"}

            res.status(500);
            res.send(response)

          } else {
            if (bodyGet.length > 0) {
                var timestamp = new Date();
                var timestamp_json = JSON.stringify(timestamp)

                if  (!password) {
                  var newDataUser = {
                    "email": email,
                    "Tittle": tittle,
                    "first_name": first_name,
                    "Tittle": tittle,
                    "DNI_Passport": DNI_Passport,
                    "Birth_date": Birth_date,
                    "Phone": Phone,
                    "gender": gender,
                    "Street_Name": Street_Name,
                    "Street_Number":Street_Number,
                    "City": City,
                    "Country": Country,
                    "Last_Conecction": timestamp
                  }

                } else {
                    var newDataUser = {
                      "email": email,
                      "Password": crypt.hash(password),
                      "Tittle": tittle,
                      "first_name": first_name,
                      "Tittle": tittle,
                      "DNI_Passport": DNI_Passport,
                      "Birth_date": Birth_date,
                      "Phone": Phone,
                      "gender": gender,
                      "Street_Name": Street_Name,
                      "Street_Number":Street_Number,
                      "City": City,
                      "Country": Country,
                      "Last_Conecction": timestamp
                    }
                  }

                var newDataUser_json = JSON.stringify(newDataUser)
                var putBody = '{"$set":' + newDataUser_json + '}'

                httpClient.put("Users?" + query + "&" + mLabAPIKey,JSON.parse(putBody),
                      function (errPUT, resMLabPUT, bodyPUT) {
                          if (errPUT || resMLabPUT.statusCode != 200){
                              res.status(500);
                              res.send({"msg" : "error bbdd actualizando usuario", "emailUsuario" : newDataUser.email})

                          } else {
                              res.status(200);

                              res.send({
                                "mensaje" : "actualizacion de datos usuario correcta",
                                "nombre": newDataUser.first_name,
                                "apellidos": newDataUser.last_name,
                                "emailUsuario" : newDataUser.email});
                            }
                      }
                )

            } else {
                res.status(404);

                var response = {" msg" : "Usuario no encontrado"};

                res.send(response);
              }
            }
        }
   )
  }

module.exports.createUsers = createUsers;
module.exports.getUserByMail = getUserByMail;
module.exports.postUpdateUsers = postUpdateUsers;
