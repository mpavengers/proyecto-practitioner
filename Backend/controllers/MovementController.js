const requestJson = require('request-json');
const jwt = require('../JWT');
const crypt = require('../crypt');
const baseMLabURL = "https://api.mlab.com/api/1/databases/proyect_techu/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getMovementsByIban(req, res){

  var iban = req.params.iban;
  var email = req.headers['email']
  var query = 'q={"IBAN":"'+ iban + '"}';
  var httpClient = requestJson.createClient(baseMLabURL);
  var token = req.headers['token'];
  var verifyToken = jwt.validarToken(token);

    if  (!iban) {
    var response = {"msg" : "Existen campos de entrada mal informados o no informados"}

    res.status(400);
    res.send(response);

    return;
  }

  if (verifyToken.status != 200) {
    res.status(verifyToken.status)
    res.send({"msg" : verifyToken.msj})

    return

  } else {
        if (email != verifyToken.email) {
        res.status(401)
        res.send({"msg" : "Error en integridad del mensaje, sesión no corresponde al usuario conectado"})

        return
      }
    }

  httpClient.get("Movements?" + query + "&" + mLabAPIKey,
        function (err, resMLab, body) {

          if (err){
              var response = {
                  "msg" : "Error obteniendo movimientos cuenta"
              }

            res.status(500);

          } else {
              if (body.length > 0){
                var response = body;

                res.status(200);
              } else {
                    var response = {
                      "msg" : "Cuenta sin movimientos"
                    }
                    res.status(200);
                }
            }
          res.send(response);
        }
  )
}

function createMovements(req, res) {

  const EUR = "EUR"
  var movement = req.body[0];
  var iban = req.body.IBAN;
  var ibandest = req.body.IBAN_Dest
  var importe = Number(req.body.Amount);
  var timestamp = new Date();
  var date = timestamp.getUTCDate(timestamp);
  var year = timestamp.getFullYear(timestamp);
  var month = timestamp.getMonth(timestamp) + 1;
  var tiempo = timestamp.toLocaleTimeString('en-ES');
  var timestamp_json = JSON.stringify(timestamp);
  var timestamp_object = JSON.parse(timestamp_json);
  var email = req.body.email;
  var Operation_latitude = Number(req.body.Operation_latitude);
  var Operation_longitude = Number(req.body.Operation_longitude);
  var newMovement = {
    "IBAN": req.body.IBAN,
    "Amount": req.body.Amount,
    "Badge": EUR,
    "Stock_Name": req.body.Stock_Name,
    "Operation_Type": req.body.Operation_Type,
    "Operation_description": req.body.Operation_description,
    "Operation_latitude": req.body.Operation_latitude,
    "Operation_longitude": req.body.Operation_longitude,
    "Date_Operation": date + "/" + month + "/" + year,
    "Time_Operation":  timestamp.toLocaleTimeString('en-ES'),
    "Timestamp": timestamp_object
  };
  var newMovement_json = JSON.stringify(newMovement);
  var httpClient = requestJson.createClient(baseMLabURL);
  var queryIban = 'q={"IBAN": "' + iban + '"}';
  var queryIbandest = 'q={"IBAN": "' + ibandest + '"}'
  var token = req.headers['token']
  var verifyToken = jwt.validarToken(token);

  if  (!iban || !importe || !req.body.Stock_Name || !req.body.Operation_Type || !req.body.Operation_description) {
    var response = {"msg" : "Existen campos de entrada mal informados o no informados"}

    res.status(400);
    res.send(response);

    return;
  }

  switch (req.body.Operation_Type) {
      case "00":

        var compra = true

        break;

      case "01":

        var reintegro = true

        break;

      case "02":

        var transferencia = true

        if (!ibandest) {
          var response = {"msg" : "Es necesario informar el IBAN"}

          res.status(400);
          res.send(response)

          return
        }

        break;

      case "03":

        var ingreso = true

        break;

      default:

        var response = {"msg" : "tipo operación incorrecta"}

        res.status(400);
        res.send(response)
        return
    }

    if (verifyToken.status != 200) {
      res.status(verifyToken.status)
      res.send({"msg" : verifyToken.msj})

      return

    } else {
          if (email != verifyToken.email) {
          res.status(401)
          res.send({"msg" : "Error en integridad del mensaje, sesión no corresponde al usuario conectado"})

          return
        }
      }

  httpClient.get("Accounts?" + queryIban + "&" + mLabAPIKey,
        function (errIban, resMLabIban, bodyIban) {

            if (errIban || resMLabIban.statusCode != 200){
              var response = {"msg" : "Error validadon cuenta"}

              res.status(500);
              res.send(response)

            } else {
                if (bodyIban.length > 0) {
                  var balance = bodyIban[0].Balance

                  if (importe > balance && !ingreso) {
                    var response = {"msg" : "Cuenta sin saldo suficiente"}

                    res.status(400)
                    res.send(response)

                  } else {
                      if (compra || reintegro || transferencia) {
                        new_balance = balance - importe

                         newMovement.Amount = newMovement.Amount * (-1)

                      } else {
                          new_balance = balance + importe
                        }

                        var putSaldo = '{"$set":{"Balance":' + new_balance + ', "Timestamp":' + timestamp_json + '}}'


                        httpClient.put("Accounts?" + queryIban + "&" + mLabAPIKey,JSON.parse(putSaldo),
                            function (errPut, resMLabPut, bodyput) {
                                if (errPut || resMLabPut.statusCode != 200){
                                  res.status(500);
                                  res.send({"msg" : "Error actualizando saldo cuenta"})

                                } else {
                                    var putMovement = '{"$set":{' + newMovement_json + '}}'

                                    httpClient.post("Movements?" + mLabAPIKey,newMovement,
                                        function (errPost, resMLabPost, bodyPost) {

                                            if (errPost || resMLabPost.statusCode != 200){
                                              res.status(500);
                                              res.send({"msg" : "Error insertando movimiento"})

                                            } else {
                                                if (transferencia) {
                                                  httpClient.get("Accounts?" + queryIbandest + "&" + mLabAPIKey,
                                                        function (errIbandest, resMLabIbandest, bodyIbandest) {
                                                            if (errIbandest || resMLabIbandest.statusCode != 200){
                                                              var response = {"msg" : "Error validadon cuenta destino"}

                                                              res.status(500);
                                                              res.send(response)

                                                            } else {
                                                                if (bodyIbandest.length > 0) {
                                                                  var balance_dest = bodyIbandest[0].Balance
                                                                  var new_balance_dest = balance_dest + importe
                                                                  var putSaldo_dest = '{"$set":{"Balance":' + new_balance_dest + ', "Timestamp":' + timestamp_json + '}}'

                                                                  httpClient.put("Accounts?" + queryIbandest + "&" + mLabAPIKey,JSON.parse(putSaldo_dest),
                                                                      function (errPutdest, resMLabPutdest, bodyputdest) {

                                                                          if (errPutdest || resMLabPutdest.statusCode != 200){

                                                                            res.status(500);
                                                                            res.send({"msg" : "Error actualizando saldo cuenta destino de la transferencia"})

                                                                          } else {
                                                                              var newMovement_dest = {
                                                                                "IBAN": ibandest,
                                                                                "Amount": req.body.Amount,
                                                                                "Badge": EUR,
                                                                                "Stock_Name": req.body.Stock_Name,
                                                                                "Operation_Type": req.body.Operation_Type,
                                                                                "Operation_description": req.body.Operation_description,
                                                                                "Operation_latitude": req.body.Operation_latitude,
                                                                                "Operation_longitude":req.body.Operation_longitude,
                                                                                "Date_Operation": date + "/" + month + "/" + year,
                                                                                "Time_Operation":  timestamp.toLocaleTimeString('en-ES'),
                                                                                "Timestamp": timestamp_object
                                                                              }

                                                                              httpClient.post("Movements?" + mLabAPIKey,newMovement_dest,
                                                                                  function (errPost_dest, resMLabPost_dest, bodyPost_dest) {

                                                                                      if (errPost_dest || resMLabPost_dest.statusCode != 200){

                                                                                        res.status(500);
                                                                                        res.send({"msg" : "Error al abonar en en la cuenta destino de la transferencia"})

                                                                                      }
                                                                                  }
                                                                              )
                                                                            }
                                                                      }
                                                                  )
                                                                }
                                                              }
                                                        }
                                                  )
                                                }

                                                res.status(200)
                                                var response = {"msg" : "Movimiento insertado en", "IBAN" : iban, "IBANdest" : ibandest }
                                                res.send(response)
                                              }
                                        }
                                    )
                                  }
                            }
                        )
                      }

                } else {
                    res.status(404);

                    var response = {" msg" : "Cuenta no encontrada"};

                    res.send(response);
                  }
              }
        }
  )
}

module.exports.createMovements = createMovements;
module.exports.getMovementsByIban = getMovementsByIban;
