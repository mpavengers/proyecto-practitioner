const requestJson = require('request-json');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const clave_token = process.env.CLAVE_TOKEN

function createToken(payload) {
  const expiration = 600

  var token=jwt.sign(payload,clave_token,{expiresIn: expiration});

   return token;
}

function validarToken(token) {
  if (!token) {
      resultado = {"status" :400,
                  "msj" : "Error en integridad del mensaje, token no informado"
                  }

  } else {
      token = token.replace('Bearer ', '');

      jwt.verify(token, clave_token, function(err, user) {
          if (err) {
            switch (err.message) {
                case "invalid token":
                  resultado = {"status" : 401,
                            "msj" : "Error en integridad del mensaje, token invalido"
                            }
                  break;

                case 'jwt expired':
                  resultado = {"status" : 401,
                          "msj" : "La sesión ha expirado, vuelva a conectarse"
                          }
                          break;

                default:
                  resultado = {"status" : 401,
                            "msj" : "Error en integridad del mensaje, token invalido"
                            }
                  break;
              }

          } else {
              resultado = {"status" : 200,
                          "msj" : "Token ok y validado",
                          "email" : user.mail
                          }
            }
          })
    }

    return resultado
}

module.exports.createToken = createToken;
module.exports.validarToken = validarToken;
