require('dotenv').config();

var cors =require('cors');
const express = require('express')
const app = express();
const port = process.env.port || 3000;
var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");

 next();
}

app.options('*',cors());
app.use(cors({exposedHeaders:['token']}));

app.use(express.json());
app.use(enableCORS)

const userController = require("./controllers/UserController")
const authController = require("./controllers/AuthController")
const accountController = require("./controllers/AccountController")
const movementController = require("./controllers/MovementController")

app.listen(port);
app.post('/proyect_techu/login',authController.postLogin);
app.post('/proyect_techu/logout/',authController.postLogout);
app.get('/proyect_techu/user/:email', userController.getUserByMail);
app.get('/proyect_techu/accounts/:email', accountController.getCuentasByMail);
app.get('/proyect_techu/movements/:iban', movementController.getMovementsByIban);
app.post('/proyect_techu/create/users',userController.createUsers);
app.post('/proyect_techu/createaccounts',accountController.createAccounts);
app.post('/proyect_techu/createmovements',movementController.createMovements);
app.post('/proyect_techu/updateusers/:email',userController.postUpdateUsers  );
